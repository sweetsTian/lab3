package com.tt.chatroom.handler;

import java.util.Iterator;
import java.util.List;

import com.tt.chatroom.server.UserEntity;
import com.tt.chatroom.thread.TaskQueue;

public class Sender {
	
	

	public static void sendToGroup(int groupID,String senderName,String content){
		List<Integer> group = GroupStorage.getGroup(groupID);
		
		String msg = Message.ChatInRoom(groupID, senderName, content);
		
		Iterator<Integer> itr = group.iterator();
		while (itr.hasNext()) {
		   // Integer receiver = itr.next();
		    int receiverId= itr.next();
		    System.out.println("Add msg to" +receiverId);
		    TaskQueue.addMsg(receiverId, msg);
		}
	}
}
