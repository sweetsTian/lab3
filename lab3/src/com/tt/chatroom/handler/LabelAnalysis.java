package com.tt.chatroom.handler;

import com.tt.chatroom.thread.Task;

public class LabelAnalysis {
	
	public static void analysis(String s){
		getClientName(s);
		getTask(s);
		getGroupName(s);
		getChatId(s);
		getContent(s);
	}
	
	
	public static void getTask(String s){
		//String[] str = s.split(":");
		if(s.startsWith("JOIN_CHATROOM")){
			Task.setTaskType(1);
		}else if(s.startsWith("LEAVE_CHATROOM")){
			Task.setTaskType(2);
		}else if(s.startsWith("DISCONNECT")){
			Task.setTaskType(3);
		}else if(s.startsWith("CHAT")){
			Task.setTaskType(4);
		}
	}


	public static String getClientName(String s){
		String name = null;
		//String[] str = s.split(":");
		if(s.startsWith("CLIENT_NAME")){
			//name= str[1].substring(2,str[1].length()-1);
			name = s.substring(14,s.length()-1);
			Task.setClientName(name);
		}
		return name;
	}
	
	public static String getGroupName(String s){
		String name = null;
		if(s.startsWith("JOIN_CHATROOM")){
			name = s.substring(16,s.length()-1);
			Task.setGroupName(name);
		}
		return name;
	}
	
	public static String getContent(String s){
		String content = null;
		if(s.startsWith("MESSAGE")){
			content = s.substring(10,s.length()-1);
			Task.setContent(content);
		}
		return content;
		
	}
	
	public static int getChatId(String s){
		int chatId = 0;
		if(s.startsWith("CHAT")){
			String str = s.substring(7,s.length()-1);
			chatId = Integer.parseInt(str);
			Task.setChatId(chatId);
		}
		return chatId;
		
	}
}
