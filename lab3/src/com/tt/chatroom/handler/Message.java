package com.tt.chatroom.handler;

public class Message {

	public static String ChatInRoom(int groupId,String sender,String content){
		String msg = null;
		msg="CHAT: [" + groupId+"]"+'\n'+"CLIENT_NAME: ["+sender+
				"]"+'\n'+"MESSAGE: ["+content+"]"+'\n'+'\n';
		return msg;
	}
	
	public static String AddRoom(int groupId,String groupName,int clientId){
		String msg = null;
		msg="JOINED_CHATROOM: [" + groupName+"]"+'\n'+"SERVER_IP: ["+0+
				"]"+'\n'+"PORT: ["+0+"]"+'\n'+"ROOM_REF: [" + groupId+"]"+'\n'+"JOIN_ID: ["
				+clientId+"]"+'\n';
		return msg;
	}
	
	public static String Error(int error_code,String des){
		String msg = null;
		msg="ERROR_CODE: [" + error_code+"]"+'\n'+"ERROR_DESCRIPTION: ["+des+
				"]"+'\n';
		return msg;
	}
	
	public static String Left(int groupId,int clientId){
		String msg = null;
		msg="LEFT_CHATROOM: [" + groupId+"]"+'\n'+"JOIN_ID: ["+clientId+
				"]"+'\n';
		return msg;
	}
	
	
}
