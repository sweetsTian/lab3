package com.tt.chatroom.handler;

import java.util.Hashtable;
import java.util.Map;

import com.tt.chatroom.server.UserEntity;

public class UserStorage {
	private static Map<Integer, UserEntity> UserList = new Hashtable<Integer, UserEntity>();
	
	public static boolean inList(int UserId){
		boolean flag = false;
		if(UserList.containsKey(UserId)){
			flag = true;
		}
		return flag;
	}
	
	public static void addUser(int userId, String clientName){
		if(!inList(userId)){
			UserEntity u = new UserEntity(userId, clientName);
			UserList.put(userId, u);
		}
	}

}
