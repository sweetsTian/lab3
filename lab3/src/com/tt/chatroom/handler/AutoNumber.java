package com.tt.chatroom.handler;

public class AutoNumber {
	private static int User_ID = 0;
	private static int Room_ID = 0;
	
	public static void addUser(){
		User_ID++;
	}
	
	public static void addRoom(){
		Room_ID++;
	}
	
	public static int getUser_ID() {
		return User_ID;
	}
	public static void setUser_ID(int user_ID) {
		User_ID = user_ID;
	}
	public static int getRoom_ID() {
		return Room_ID;
	}
	public static void setRoom_ID(int room_ID) {
		Room_ID = room_ID;
	}
	
	

}
