package com.tt.chatroom.handler;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.tt.chatroom.server.UserEntity;


public class GroupStorage {
	//private List<UserEntity> userList = new Vector<UserEntity>();
	@SuppressWarnings("rawtypes")
	private static Map<Integer, List> groupMap = new Hashtable<Integer, List>();
	private static Map<String, Integer> groupIDMap = new Hashtable<String, Integer>();
	

	@SuppressWarnings("rawtypes")
	public static Map<Integer, List> getMsgMap() {
		return groupMap;
	}

	@SuppressWarnings("rawtypes")
	public static List getGroup(int groupNumber) {
		return groupMap.get(groupNumber);
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Add a user to a group, if the group is exit add it.
	 * if the group is not exit then add one and add the user in it.
	 */
	public static String addMember(String groupName, int userId){
		int groupId = 0;
		if(groupIDMap.containsKey(groupName)){
			groupId = groupIDMap.get(groupName);
			if(!groupMap.get(groupId).contains(userId))
			    groupMap.get(groupId).add(userId);
			}
		else{
			AutoNumber.addRoom();
		    groupId = AutoNumber.getRoom_ID();
			groupIDMap.put(groupName, groupId);
			List<Integer> group = new Vector<Integer>();
			group.add(userId);
			groupMap.put(groupId, group);
		}
		String s = Message.AddRoom(groupId, groupName, userId);
		//show();
		return s;
	}
	
	/**
	 * Remove user from the group
	 * If the group is exit and the user in the group
	 */
	public static String leaveGroup(String groupName,int userId){
		int groupId = 0;
		if(groupIDMap.containsKey(groupName)){
			groupId=groupIDMap.get(groupName);
			if(groupMap.get(groupId).contains(userId)){
				groupMap.get(groupId).remove(userId);
			}
		}
		String s = Message.Left(groupId, userId);
		//show();
		return s;
	}
	public static void show(){
		List<Integer> list = groupMap.get(1);
		int  i = list.size();
		for(int j=0;j<i;j++){
		System.out.println(list.get(j)+"LLLLLLLLLLList");
		}
	}
	
	public static void removeMember(String groupNumber, UserEntity member){
		groupMap.get(groupNumber).remove(member);
	} 	
}
