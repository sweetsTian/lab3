package com.tt.chatroom.thread;

import java.net.Socket;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * To store the task(Runnable)
 * @author Tian
 */
public class TaskQueue {
	
//	private static BlockingQueue<Runnable> taskQueue = new 
//			LinkedBlockingQueue<Runnable>();
	private static Map<Integer, Task> taskQueue = new Hashtable<Integer, Task>();
	//private static int task_number=0;
	
	/**
	 * Put a task to the taskQueue and wait idle thread to do it
	 */
	public static void execute(Task task,int ID){
		synchronized (taskQueue) {
			//System.out.println("add task~~~~~~~");
			//System.out.println("task_number is " + task_number+ "then add one");
			//task_number++;
			taskQueue.put(ID, task);
			//taskQueue.offer(task);
			taskQueue.notify();
		}
	}
	
	public static Task poll(int ID) {
		// TODO Auto-generated method stub
		//System.out.println("poll task~~~~~~~~~~~~");
		return taskQueue.get(ID);
	}
	
	public static void clear() {
		// TODO Auto-generated method stub
		taskQueue.clear();
	}
	
	public static boolean isEmpty() {
		// TODO Auto-generated method stub

		return taskQueue.isEmpty();
	}

	public static void waits(int i) throws InterruptedException{
		try{
			taskQueue.wait(i);
		}catch(Exception e){

		}
	}
	
	public static void addMsg(int userId,String msg){
		System.out.println("add to"+userId+"aaaaaaaaaaaaaaaaadd"+msg);
		 taskQueue.get(userId).addMsg(msg);
		 
//		task.addMsg(msg);
		
	}
	
//	public static int size(){
//		return task_number;
//	}
	
}
