package com.tt.chatroom.thread;


/**
 * Threads get task from taskQueue and execute it
 * @author Tian
 */
public class Threads extends Thread{

	int i; 
	
	
	public Threads(int i){
		this.i = i ;
		//System.out.println("i: "+i);
	}
	
	private boolean isRunning = true;
	
	public void run(){
		Task r = null;
		
		while(isRunning){
			
			synchronized (TaskQueue.class) {
				while(isRunning&&TaskQueue.isEmpty()){
					try{
						TaskQueue.waits(20);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				if(!TaskQueue.isEmpty())
					//System.out.println("Thread "+ i+" is working");
					r = TaskQueue.poll(i);
			}
			if(r!=null){
				
				r.run();
			}
			r=null;
		}
	}
	
	public void stopWorker(){
		isRunning = false;
	}
	
}
