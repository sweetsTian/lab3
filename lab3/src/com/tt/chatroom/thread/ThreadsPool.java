package com.tt.chatroom.thread;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tt.chatroom.server.Server;

public class ThreadsPool {

	// The number of threads
	private static int worker_num = 4;
	// work thread group
	//private static List<Threads> workThreads = new ArrayList<Threads>();
	private static Map<Integer, Threads> workThreads = new Hashtable<Integer, Threads>();
	//
	//private static volatile int finished_task = 0;
	// Task Queue, the inqueue actually, it store the user requirement, the
	// LinkedBlockingQueue is a safe threads
	// private static BlockingQueue<Task> taskQueue = new
	// LinkedBlockingQueue<Task>();
	private static ThreadsPool threadsPool;
	private static boolean thread_Status = false;

	public static boolean isThread_Status() {
		return thread_Status;
	}

	public static void changeThreadStatus() {
		if (thread_Status == false)
			thread_Status = true;
		else
			thread_Status = false;
	}

	public static void StartWorkThread() {
		if (!thread_Status) {
			changeThreadStatus();
//			//workThreads = new Threads[worker_num];
//			for (int i = 0; i < worker_num; i++) {
//				Threads t = new Threads(i);
//				t.start();
//				//workThreads.add(t);
//				//workThreads.
//				// begin the work thread
//			}
		}
	}
	
	public static void addThreads(int UserId){
		Threads t = new Threads(UserId);
		t.start();
		workThreads.put(UserId, t);
	}
	
//	public static void addThread(){
//		System.out.println("add threads");
//		worker_num++;
//		Threads t = new Threads(worker_num);
//		t.start();
//		workThreads.add(t);
//		
//	}
//	public static void reduceThread(){
//		worker_num--;
//		workThreads.get(worker_num).stopWorker();
//		workThreads.remove(worker_num);
//		
//	}



	/**
	 * get the number of work threads
	 * */
	public int getWorkThreadNumber() {
		return worker_num;
	}


	public static void adjust(){
		
//		if(Server.getConn()<(worker_num/2)){
//			reduceThread();
//		}else if(Server.getConn()>=worker_num){
//			addThread();
//		}
	}
	
	public static void destroy() {
		
		try {
//			Iterator<Threads> it = workThreads.iterator();
//			while(it.hasNext()){
//				Threads value = it.next();
//				value.stopWorker();
//				it.remove();
//			}
			thread_Status = false;
			Server.closeAll();
			System.out.println("destory threadspool");
			TaskQueue.clear(); // clear the taskQueue
			System.exit(0);
		} catch (Exception e) {

		}
	}
	
	public static boolean isRunning(){
		return thread_Status;
	}

}
