package com.tt.chatroom.thread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.tt.chatroom.handler.LabelAnalysis;
import com.tt.chatroom.handler.UserStorage;

public class Task  {

	private Socket socket = null;
	private String changedSentence;
	private BufferedReader inFromClient = null;
	//private DataOutputStream outToClient = null;
	private PrintWriter pw = null;
	private List<String> msg = new Vector<String>();
	private static String output = null;
	private static int workTpye = 0;
	private static String clientName = null;
	private static String groupName= null;
	private static int chatId;
	private static String content =null;
	
	//private static int cou=0;

	public static void setTaskType(int workTpye) {
		Task.workTpye = workTpye;
	}
	
	public static void setChatId(int chatId) {
		Task.chatId = chatId;
	}

	public static void setClientName(String clientName) {
		Task.clientName = clientName;
	}
	
	public static void setContent(String content) {
		Task.content = content;
	}
	
	public static void setGroupName(String groupName) {
		Task.groupName = groupName;
	}

	private int ID;
	public Task(Socket socket,int ID) {
		super();
		this.socket = socket;
		this.ID=ID;
	}
	
	public void addMsg(String s){
		msg.add(s);
	}

	@SuppressWarnings("static-access")
	public void run() {
		// TODO Auto-generated method stub
		try {
		inFromClient = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		//outToClient = new DataOutputStream(socket.getOutputStream());
		pw = new PrintWriter(new 
			BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
		
			if (socket.isClosed()) {
				socket=null;
			} else {
				Thread receiver = new Thread(){
				//clientSentence = inFromClient.readLine();
					public void run(){
				if (!inFromClient.equals(null)) {
					try {
						while ((output = inFromClient
								.readLine()) != null) {
							try {
								if(output.equals("")){
									break;
								}
								LabelAnalysis.analysis(output);
								//System.out.println(output);
							} catch (Exception e) {
								e.printStackTrace();
								// inFromServer.close();
								System.out
										.println("Exception~~~~~");
								break;
							}
							UserStorage.addUser(ID, clientName);
							String message = null;
							if(workTpye==1){
								message = Work.joinRoom(ID, groupName);
							}else if(workTpye==2){
								message = Work.leaveRoom(ID, groupName);
							}else if(workTpye==3){
								Work.disconnect();
							}else if(workTpye==4){
								//System.out.println("CCCCCCChat");
								Work.chat(chatId, clientName, content);
							}
							addMsg(message);
							message = null;
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
					}
				};

				Thread sender = new Thread(){
				
				public void run(){
				
				Iterator itr = msg.iterator();
				System.out.println("rrrrrrun sender");
				while (itr.hasNext()) {
					System.out.println("TTTTTTTest after while");
					changedSentence = (String) itr.next();
				    pw.println(changedSentence);
				    pw.flush();
				    itr.remove();
				}
				}
				//ThreadsPool.adjust();
			};
			
			receiver.run();
			sender.run();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			try {

				inFromClient.close();
				pw.close();
				socket.close();

			} catch (IOException e1) {
				
			}

		} finally {
			try {

				// inFromClient.close();
				// outToClient.close();
				// socket.close();
				if (socket != null) {
					TaskQueue.execute(new Task(socket,ID),ID);
				}

			} catch (Exception e1) {
				
			}
		}

	}
	// }

}
