package com.tt.chatroom.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.tt.chatroom.handler.AutoNumber;
import com.tt.chatroom.thread.Task;
import com.tt.chatroom.thread.TaskQueue;
import com.tt.chatroom.thread.ThreadsPool;

//import org.jsoup.helper.Validate;

public class Server {

	private static ServerSocket s = null;
	private static Socket socket = null;

	public static void main(String[] args) {
		if (args.length == 1) {
			// TODO Auto-generated method stub

			// Validate.isTrue(args.length>=2, "Error: Parameters Error");
			 String arg = args[0];
			int port = 9675;
			try {
				port = Integer.parseInt(arg);
			} catch (Exception e) {
				System.err.println("Parameters Error,use default port 8080");
			}
			ThreadsPool.StartWorkThread();
			System.out.println("Server start ....");
			try {
				int userID;
				s = new ServerSocket(port);
				while (true) {
					// System.out.println(ThreadsPool.isRunning() +
					// "aaaaaaaaaa");

					//System.out.println("tttttttest");
					if (ThreadsPool.isRunning()) {
						//System.out.println("POOOOOOOOOOOOOOOOO");
						socket = s.accept();
						AutoNumber.addUser();
						userID=AutoNumber.getUser_ID();
						//System.out.println("UserId is:"+userID);
						ThreadsPool.addThreads(userID);
						Task task = new Task(socket,userID);
						TaskQueue.execute(task,userID);
						//System.out.println(conn+"fff");
					} else {

						break;
					}
				}

			} catch (Exception e) {
				try {
					s.close();
					socket.close();
				} catch (IOException e1) {
					//e1.printStackTrace();
				}
			}
		}else{
			System.out.println("Parameters Error");
		}
	}

//	public static int getConn() {
//		return userID;
//	}

	public static void closeAll() {
		try {
			s.close();
			socket.close();
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

}
