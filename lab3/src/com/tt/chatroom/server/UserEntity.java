package com.tt.chatroom.server;

import java.net.InetAddress;

public class UserEntity {

	private int JOIN_ID;
	private String CLIENT_NAME;
	//private InetAddress IPAddress;
	//private int Port;
	
	public UserEntity(int jOIN_ID, String cLIENT_NAME) {
		super();
		JOIN_ID = jOIN_ID;
		CLIENT_NAME = cLIENT_NAME;
		//IPAddress = iPAddress;
		//Port = port;
	}
	
	public int getJOIN_ID() {
		return JOIN_ID;
	}
	public void setJOIN_ID(int jOIN_ID) {
		JOIN_ID = jOIN_ID;
	}
	public String getCLIENT_NAME() {
		return CLIENT_NAME;
	}
	public void setCLIENT_NAME(String cLIENT_NAME) {
		CLIENT_NAME = cLIENT_NAME;
	}
}
