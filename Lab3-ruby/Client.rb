require 'socket'
require 'thread'
require 'json'
class Client
  def init(port)
    @socket = UDPSocket.new
    @server_port = 8080
    @node_port = port
    @node_ip = getIp
    @client_name = nil
    @join_id = nil
    @socket.bind(nil,@node_port)
    @work_q = Queue.new

    self.user_input
    self.node_listener()
    self.worker

  end

  def user_input
    Thread.new{
      loop do
        puts "\nIf you want join a chatroom please input \"JOINING CHATROOM\" .If you want leave a chatroom please input \"LEAVE CHATROOM\".If you want chat please input \"CHAT\".If you want disconnect please input \"DISCONNECT\"."
        user_wants = gets.chomp
        user_wants = user_wants.to_s
        #puts user_wants
        case user_wants
          when "JOINING CHATROOM" then
            join_chatroom
          when "LEAVE CHATROOM" then
            leave_chatroom
          when "CHAT" then
            chat
          when "DISCONNECT" then
            disconnect
            exit
          else
            puts "Wrong input, please input again!"
        end
      end

    }
  end
  def node_listener
    Thread.new{
      loop do
        #puts "open listener"
        data, sender = @socket.recvfrom(65536)
        #puts data
        data = JSON.parse data # parse data to hash format
        @work_q.push data # add request to the queue
       # puts data
      end
    }
  end

  def worker
    workers = (0...4).map do
      Thread.new do
        loop do
          while !@work_q.empty?
            msg = @work_q.pop(true)
            # handle the message get from other peers
            case msg["type"]

              when "JOINING CHATROOM RESPONSE" then
                if msg["result"]
                  if @join_id.equal?(nil)
                    @join_id=msg["join_id"]
                  end
                  puts "Join the chatroom successful!"
                else
                  puts msg["content"]
                end
              when "LEAVE CHATROOM RESPONSE" then
                if msg["result"]
                  puts "Leave the chatroom successful！"
                else
                  puts msg["content"]
                end
              when "CHAT RESPONSE" then
                if msg["result"]
                  puts "Send chat message successful!"
                else
                  puts msg["content"]
                end
              when"CHAT" then
                puts "*****************************************************\nGet chat message from \"#{msg["sender"]}\" in chatroom \"#{msg["chatroom_name"]}\"\nMessage is \"#{msg["message"]}\"\n*****************************************************"
               # puts msg
              when "DISCONNECT RESPONSE" then
                #puts msg
                if msg["result"]
                  puts "Disconnect successful! Close the client...."
                  abort("Message goes here")
                else
                  puts "Disconnect fail, please try again!"
                end

            end

          end
        end
      end
    end
    workers.map(&:join)
  end

  def send_udp_msg(msg)

    #puts msg
    @socket.send(msg.to_json, 0, 'localhost', @server_port)

  end

  def join_chatroom
    if @client_name.equal?(nil)
      puts "please type client name:"
      @client_name = gets.chomp.to_s
    end
    puts "please type chatroom name"
    chatroom_name = gets.chomp.to_s
    msg = {"type"=>"JOINING CHATROOM","client_ip"=>@node_ip,"client_port"=>@node_port,"client_name"=>@client_name,"join_id"=>@join_id,"chatroom_name"=>chatroom_name}
   # puts msg
    send_udp_msg msg
  end

  def leave_chatroom
    puts "please type chatroom name"
    chatroom_name = gets.chomp.to_s
    msg = {"type"=>"LEAVE CHATROOM","join_id"=>@join_id,"chatroom_name"=>chatroom_name,"client_port"=>@node_port,"client_name"=>@client_name,"client_ip"=>@node_ip}
    send_udp_msg msg
  end

  def chat
    puts "please type chatroom name:"
    chatroom_name = gets.chomp.to_s
    puts "Please type chat content:"
    text = gets.chomp.to_s
    msg = {"type"=>"CHAT","join_id"=>@join_id,"chatroom_name"=>chatroom_name,"message"=>text,"client_port"=>@node_port,"client_name"=>@client_name,"client_ip"=>@node_ip}

    send_udp_msg msg
  end

  def disconnect
    puts "Disconnecting please wait......"
    msg = {"type"=>"DISCONNECT","join_id"=>@join_id,"client_ip"=>@node_ip,"client_port"=>@node_port}
    #puts msg
    send_udp_msg msg
    end

  def getIp
    ip = UDPSocket.open {|s| s.connect("64.233.187.99", 1); s.addr.last}
    ip
  end


end

c = Client.new
puts "Please input the port of client: "
port = gets.chomp.to_i
c.init port