require 'socket'
require 'thread'
require 'json'
Client_node = Struct.new(:ip, :port)
class Server
  class << self
    def init()
      @chat_record = Hash.new
      @socket = UDPSocket.new
      @server_ip = getIp
      @server_port = 8080
      @socket.bind(nil,@server_port)
      @work_q = Queue.new
      @auto_number = 1

      puts "Sever is open.."

      self.node_listener()
      self.worker

    end

    def getIp
      ip = UDPSocket.open {|s| s.connect("64.233.187.99", 1); s.addr.last}
      ip
    end
    def node_listener
      Thread.new{
        loop do
          data, sender = @socket.recvfrom(65536)
          data = JSON.parse data # parse data to hash format
          @work_q.push data # add request to the queue
           #puts data
        end
      }
    end

    def worker
      workers = (0...4).map do
        Thread.new do
          loop do
            while !@work_q.empty?
              msg = @work_q.pop
              # handle the message get from other peers
              case msg["type"]

                when "JOINING CHATROOM" then
                  client_port = msg["client_port"]
                  client_ip = msg["client_ip"]
                  #puts "client_port: #{client_port}"
                  join_id=msg["join_id"]
                  if join_id.equal?(nil)
                    join_id = @auto_number
                    @auto_number= @auto_number+1
                    #puts "auto_number #{@auto_number}"
                  end

                  msg_result = join_chatroom msg["chatroom_name"],client_ip,client_port,join_id
                  send_udp_msg msg_result,client_ip,client_port
                when "LEAVE CHATROOM" then
                  #puts "test leave chatroom"
                  join_id = msg["join_id"]
                  if join_id.equal?(nil)
                    msg_result = {"type"=>"LEAVE CHATROOM RESPONSE","result"=>false,"content"=>"Please join a chatroom first!"}
                  else
                    chatroom_name=msg["chatroom_name"]
                    client_port = msg["client_port"]
                    client_ip = msg["client_ip"]
                    msg_result = leave_chatroom chatroom_name,join_id
                  end

                  send_udp_msg msg_result,client_ip,client_port

                when "CHAT" then
                  #puts "test chat"
                  join_id= msg["join_id"]
                  chatroom_name=msg["chatroom_name"]
                  client_port = msg["client_port"]
                  client_ip = msg["client_ip"]
                  client_name = msg["client_name"]
                  content = msg["message"]
                  #puts "test chat values #{chatroom_name} and #{client_port} and #{client_ip} and #{client_name} and #{join_id}"
                  msg_result = chat_response chatroom_name,join_id
                  send_udp_msg msg_result,client_ip,client_port
                  if msg_result["result"]
                    send_chat_message content,chatroom_name,client_name
                  end

                when "DISCONNECT"then
                  join_id= msg["join_id"]
                  client_port = msg["client_port"]
                  client_ip = msg["client_ip"]
                  msg_result = disconnect join_id
                  send_udp_msg msg_result,client_ip,client_port
              end

            end
          end
        end
      end
      workers.map(&:join)
    end

    def join_chatroom( chatroom_name,client_ip,client_port,join_id)
      msg_result = nil
      if @chat_record.has_key?(chatroom_name)
        members = @chat_record.fetch(chatroom_name)
        #puts "test join_id #{join_id}"
        if members.has_key?(join_id)
          msg_result  = {"type"=>"JOINING CHATROOM RESPONSE","result"=>false,"content"=>"You already join the #{chatroom_name}","join_id"=>join_id,"server_ip"=>@server_ip,"server_port"=>@server_port}
        else
          members.store(join_id,Client_node.new(client_ip,client_port))
          msg_result  = {"type"=>"JOINING CHATROOM RESPONSE","result"=>true,"join_id"=>join_id,"server_ip"=>@server_ip,"server_port"=>@server_port}
        end
      else
        members_hash = Hash.new
        members_hash.store(join_id,Client_node.new(client_ip,client_port))
        @chat_record.store(chatroom_name,members_hash)
        msg_result  = {"type"=>"JOINING CHATROOM RESPONSE","result"=>true,"join_id"=>join_id,"server_ip"=>@server_ip,"server_port"=>@server_port}
      end
     # puts @chat_record
      return msg_result
    end

    def leave_chatroom(chatroom_name,join_id)
      msg_result=nil
      if @chat_record.has_key?(chatroom_name)
        members = @chat_record.fetch(chatroom_name)
        if members.has_key?(join_id)
          members.delete(join_id)
          msg_result  = {"type"=>"LEAVE CHATROOM RESPONSE","result"=>true,"join_id"=>join_id}
        else
          msg_result  = {"type"=>"LEAVE CHATROOM RESPONSE","result"=>false,"content"=>"You did not join the chatroom #{chatroom_name}!"}
        end
      else
        msg_result  = {"type"=>"LEAVE CHATROOM RESPONSE","result"=>false,"content"=>"Sorry does not exist the chatroom #{chatroom_name}!"}
      end
     # puts @chat_record
      return msg_result
    end

    def send_chat_message(message,chatroom_name,sender_name)
      if @chat_record.has_key?(chatroom_name)
        members = @chat_record.fetch(chatroom_name)
        msg = {"type"=>"CHAT","message"=>message,"chatroom_name"=>chatroom_name,"sender"=>sender_name}
        members.each_value { |value|
          ip = value["ip"]
          port = value["port"]
          send_udp_msg msg,ip,port
        }
      end
    end

    def disconnect(join_id)
      @chat_record.each_value{|members|
        members.delete(join_id)
      }
      msg = {"type"=>"DISCONNECT RESPONSE","result"=>true}
      msg
    end

    def chat_response (chatroom_name,join_id)
      #puts "chat_response test"
      msg_result=nil
      if @chat_record.has_key?(chatroom_name)
        members = @chat_record.fetch(chatroom_name)
        if members.has_key?(join_id)
          msg_result  = {"type"=>"CHAT RESPONSE","result"=>true,"client_name"=>join_id}
        else
          msg_result  = {"type"=>"CHAT RESPONSE","result"=>false,"content"=>"You did not join the chatroom #{chatroom_name}!"}
        end
      else
        msg_result  = {"type"=>"CHAT RESPONSE","result"=>false,"content"=>"Sorry does not exist the chatroom #{chatroom_name}!"}
      end
     # puts msg_result
      return msg_result
    end

    def send_udp_msg(msg,ip,port)
      receiver_ip = ip
      receiver_port =port
     # puts msg
     # puts receiver_port
      @socket.send(msg.to_json, 0, 'localhost', receiver_port)

    end

  end

end
Server.init